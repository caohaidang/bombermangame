package uet.oop.bomberman.gui;

import uet.oop.bomberman.Game;

import javax.swing.*;
import java.awt.*;

/**
 * Swing Panel hiển thị thông tin thời gian, điểm mà người chơi đạt được
 */
public class InfoPanel extends JPanel {
	
	private JLabel timeLabel;
	private JLabel pointsLabel;
	private JLabel livesLabel;
        private JLabel levelLabel;

	public InfoPanel(Game game) {
		setLayout(new GridLayout());
		
		timeLabel = new JLabel("Time: " + game.getBoard().getTime());
		timeLabel.setForeground(Color.black);
		timeLabel.setHorizontalAlignment(JLabel.CENTER);
		
		pointsLabel = new JLabel("Points: " + game.getBoard().getPoints());
		pointsLabel.setForeground(Color.black);
		pointsLabel.setHorizontalAlignment(JLabel.CENTER);

		livesLabel = new JLabel("Lives: " + game.getBoard().getLives());
		livesLabel.setForeground(Color.black);
		livesLabel.setHorizontalAlignment(JLabel.CENTER);
                
        levelLabel = new JLabel("Level: " + game.getBoard().getLevel());
		levelLabel.setForeground(Color.black);
		levelLabel.setHorizontalAlignment(JLabel.CENTER);

		add(timeLabel);
		add(pointsLabel);
		add(livesLabel);
        add(levelLabel);
		
		setBackground(Color.white);
		setPreferredSize(new Dimension(0, 40));
	}
	
	public void setTime(int t) {
		timeLabel.setText("Time: " + t);
	}

	public void setPoints(int t) {
		pointsLabel.setText("Score: " + t);
	}

	public void setLives(int t) {
		livesLabel.setText("Lives: " + t);

	}
        public void setLevel(int t) {
                levelLabel.setText("Level: " + t);
        }
}
